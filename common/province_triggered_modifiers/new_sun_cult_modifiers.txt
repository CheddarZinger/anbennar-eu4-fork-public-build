
# Province Triggered modifiers are here.
# These are added to provinces through the add_province_triggered_modifier effect
#
# Effects are fully scriptable here.

bulwar_incident_daily_pulse = {
	potential = {
		always = yes
	}
	
	hidden = yes

	trigger = {
		NOT = { has_province_flag = nsc_pledge_score_calculation_done }
	}
	
	on_activation = {
		set_province_flag = nsc_pledge_score_calculation_done
		F21 = { nsc_calculate_pledge_score = yes }	#Birsartanses
		F25 = { nsc_calculate_pledge_score = yes }	#Sareyand
		F34 = { nsc_calculate_pledge_score = yes }	#Azka-Evran
		F37 = { nsc_calculate_pledge_score = yes }	#Irrliam
		F42 = { nsc_calculate_pledge_score = yes }	#Varamhar
	}

	
	on_deactivation = {
		clr_province_flag = nsc_pledge_score_calculation_done
	}
}

nsc_amussu_triggered_modifier = {
	potential = {
		always = yes
	}
	
	#hidden = yes

	trigger = {
		num_of_units_in_province = { who = event_target:has_amussu_incident_target amount = 1 }
		# OR = {
			# AND = {
				# unit_in_battle = yes
				# num_of_units_in_province = { who = REB amount = 1 }
			# }
			# AND = {
				# event_target:has_amussu_incident_target = { NOT = { has_country_flag = nsc_amussu_battle_refused } }
				# NOT = { has_province_modifier = nsc_amussu_battle_daily_pulse }
			# }
		# }
	}
	
	on_activation = {
		event_target:has_amussu_incident_target = { country_event = { id = new_sun_cult.283 } }
	}
	
	on_deactivation = {
		event_target:has_amussu_incident_target = { clr_country_flag = nsc_amussu_battle_refused }
	}
}

nsc_amussu_battle_daily_pulse = {	#spawns reinforcements and check if the battle has ended
	potential = {
		always = yes
	}
	
	#hidden = yes
	
	trigger = {
		NOT = { has_province_flag = nsc_amussu_battle_tick }
	}
	
	on_activation = {
		if = {
			limit = {
				unit_in_battle = yes
				num_of_units_in_province = { who = event_target:has_amussu_incident_target amount = 1 }
				num_of_units_in_province = { who = REB amount = 1 }
			}
			set_province_flag = nsc_amussu_battle_tick
			change_variable = { which = nscAmussuBattleTickVar value = 1 }
			if = {
				limit = {
					check_variable = { which = nscAmussuStrenghVar value = 1 }
					check_variable = { which = nscAmussuBattleTickVar value = 10 }
				}
				change_variable = { which = nscAmussuStrenghVar value = -1 }
				set_variable = { which = nscAmussuBattleTickVar value = 0 }
				spawn_amussu_rebel_stack = { type = amussu_reinforcement_rebels }
			}
		}
		else = {
			province_event = { id = new_sun_cult.284 }	#battle ends
		}
	}
	
	on_deactivation = {
		if = {
			limit = {
				unit_in_battle = yes
				num_of_units_in_province = { who = event_target:has_amussu_incident_target amount = 1 }
				num_of_units_in_province = { who = REB amount = 1 }
			}
			clr_province_flag = nsc_amussu_battle_tick
			change_variable = { which = nscAmussuBattleTickVar value = 1 }
			if = {
				limit = {
					check_variable = { which = nscAmussuStrenghVar value = 1 }
					check_variable = { which = nscAmussuBattleTickVar value = 10 }
				}
				change_variable = { which = nscAmussuStrenghVar value = -1 }
				set_variable = { which = nscAmussuBattleTickVar value = 0 }
				spawn_amussu_rebel_stack = { type = amussu_reinforcement_rebels }
			}
		}
		else = {
			province_event = { id = new_sun_cult.284 }	#battle ends
		}
	}
}

amussu_incident_monthly_pulse = {
	potential = {
		always = yes
	}
	
	#hidden = yes

	trigger = {
		OR = {
			AND = {
				is_month = 0
				NOT = { is_month = 1 }
			}
			AND = {
				is_month = 2
				NOT = { is_month = 3 }
			}
			AND = {
				is_month = 4
				NOT = { is_month = 5 }
			}
			AND = {
				is_month = 6
				NOT = { is_month = 7 }
			}
			AND = {
				is_month = 8
				NOT = { is_month = 9 }
			}
			AND = {
				is_month = 10
				NOT = { is_month = 11 }
			}
		}
	}
	
	on_activation = {
		if = { limit = { NOT = { has_province_modifier = nsc_amussu_battle_daily_pulse } }
			nsc_amussu_moves_effect = yes
			province_event = { id = new_sun_cult.284 }
		}
	}
	
	on_deactivation = { }
}
